#!/usr/bin/env python3
import sys, json, re, requests
from html.parser import HTMLParser
from typing import Union, List, Tuple, Dict, Any, Optional

URL = "https://ephinea.pioneer2.net/drop-charts/"

# List of IDs
IDS = ["Viridia",
       "Greenill",
       "Skyly",
       "Bluefull",
       "Purplenum",
       "Pinkal",
       "Redria",
       "Oran",
       "Yellowboze",
       "Whitill"]

# List of difficulties
DIFFICULTIES = ["Normal", "Hard", "Very Hard", "Ultimate"];

# Map of monster to area
AREAS_MAP = {
    1: {
        "Forest": [
            "Hildebear",
            "Hildelt",
            "Hildeblue",
            "Hildetorr",
            "Mothmant",
            "Mothvert",
            "Monest",
            "Mothvist",
            "Rag Rappy",
            "El Rappy",
            "Al Rappy",
            "Pal Rappy",
            "Savage Wolf",
            "Gulgus",
            "Barbarous Wolf",
            "Gulgus-Gue",
            "Booma",
            "Bartle",
            "Gobooma",
            "Barble",
            "Gigobooma",
            "Tollaw",
            "Dragon",
            "Sil Dragon"
        ],
        "Cave": [
            "Grass Assassin",
            "Crimson Assassin",
            "Poison Lily",
            "Ob Lily",
            "Nar Lily",
            "Mil Lily",
            "Nano Dragon",
            "Evil Shark",
            "Vulmer",
            "Pal Shark",
            "Govulmer",
            "Guil Shark",
            "Melqueek",
            "Pofuilly Slime",
            "Pouilly Slime",
            "Pan Arms",
            "Migium",
            "Hidoom",
            "De Rol Le",
            "Dal Ral Lie"
        ],
        "Mine": [
            "Dubchic",
            "Dubchich",
            "Gilchic",
            "Gilchich",
            "Garanz",
            "Baranz",
            "Sinow Beat",
            "Sinow Blue",
            "Sinow Gold",
            "Sinow Red",
            "Canadine",
            "Canabin",
            "Canane",
            "Canune",
            "Vol Opt",
            "Vol Opt ver. 2"
        ],
        "Ruins": [
            "Delsaber",
            "Chaos Sorcerer",
            "Gran Sorcerer",
            "Dark Gunner",
            "Death Gunner",
            "Chaos Bringer",
            "Dark Bringer",
            "Dark Belra",
            "Indi Belra",
            "Claw",
            "Bulk",
            "Bulclaw",
            "Dimenian",
            "Arlan",
            "La Dimenian",
            "Merlan",
            "So Dimenian",
            "Del-D",
            "Dark Falz"
        ]
    },
    2: {
        "VR Temple": [
            "Hildebear",
            "Hildelt",
            "Hildeblue",
            "Hildetorr",
            "Mothmant",
            "Mothvert",
            "Monest",
            "Mothvist",
            "Rag Rappy",
            "El Rappy",
            "Love Rappy",
            "Halo Rappy",
            "Egg Rappy",
            "St. Rappy",
            "Grass Assassin",
            "Crimson Assassin",
            "Poison Lily",
            "Ob Lily",
            "Nar Lily",
            "Mil Lily",
            "Dark Belra",
            "Indi Belra",
            "Dimenian",
            "Arlan",
            "La Dimenian",
            "Merlan",
            "So Dimenian",
            "Del-D",
            "Barba Ray"
        ],
        "VR Spaceship": [
            "Savage Wolf",
            "Gulgus",
            "Barbarous Wolf",
            "Gulgus-Gue",
            "Pan Arms",
            "Migium",
            "Hidoom",
            "Dubchic",
            "Dubchich",
            "Gilchic",
            "Gilchich",
            "Garanz",
            "Baranz",
            "Delsaber",
            "Chaos Sorcerer",
            "Gran Sorcerer",
            "Gol Dragon"
        ],
        "Central Control Area": [
            "Merillia",
            "Meriltas",
            "Gee",
            "Gi Gue",
            "Mericarol",
            "Merikle",
            "Mericus",
            "Ul Gibbon",
            "Zol Gibbon",
            "Gibbles",
            "Sinow Berill",
            "Sinow Spigell",
            "Gal Gryphon"
        ],
        "Seabed": [
            "Dolmolm",
            "Dolmdarl",
            "Morfos",
            "Recon",
            "Sinow Zoa",
            "Sinow Zele",
            "Deldepth",
            "Delbiter",
            "Olga Flow"
        ],
        "Control Tower": [
            "Mericarol",
            "Merikle",
            "Mericus",
            "Gibbles",
            "Recon",
            "Delbiter",
            "Gi Gue",
            "Ill Gill",
            "Del Lily",
            "Epsilon"
        ]
    },
    4: {
        "Crater": [
            "Boota",
            "Ze Boota",
            "Ba Boota",
            "Dorphon",
            "Dorphon Eclair",
            "Sand Rappy",
            "Del Rappy",
            "Satellite Lizard",
            "Yowie",
            "Zu",
            "Pazuzu",
            "Astark"
        ],
        "Desert": [
            "Sand Rappy",
            "Del Rappy",
            "Satellite Lizard",
            "Yowie",
            "Zu",
            "Pazuzu",
            "Goran",
            "Pyro Goran",
            "Goran Detonator",
            "Merissa A",
            "Merissa AA",
            "Girtablulu",
            "Saint Million",
            "Shambertin",
            "Kondrieu"
        ]
    }
}

# Regex to extract the odds from a title of a drop rate.
RGX_RATE = re.compile(
    r'Drop Rate:'
    + r' 1/(\d*(?:\.\d*)?)'
    + r' \([\d\.%]+\)\r'
    + r'Rare Rate:'
    + r' 1/(\d*(?:\.\d*)?)'
    + r' \([\d\.%]+\)\r'
)

# Regex to extract the episode from a table title.
RGX_EPISODE = re.compile(r'EPISODE (\d)')

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def get_areas(monster: str, episode: int) -> List[str]:
    """ Returns the area the given monster is found in. """
    areas = []
    if episode not in [1, 2, 4]:
        return areas
    if "boxes" in monster:
        return [monster[0: monster.index(" boxes")]]
    for area, monsters in AREAS_MAP[episode].items():
        if monster in monsters:
            areas.append(area)
    return areas

def get_attr(attrs: List[Tuple[str, str]], key: str) -> Optional[str]:
    """ Returns the value of an attribute from given attributes list. """
    for attr in attrs:
        (attrname, value) = attr
        if attrname == key:
            return value
    return None

class DropChartParser(HTMLParser):
    """ Allows to parse an Ephinea drop chart table. """

    def __init__(self, difficulty: str):
        super().__init__()

        self.log_next = False
        self.in_table = False
        self.current_tag = ""
        self.new_row = False
        self.wide_row = False
        self.new_table = False
        self.box_table = False
        self.next_is_monster_name = False

        self.difficulty = difficulty
        self.current_episode = 1
        self.current_monster = ""
        self.column = -1
        self.current_drop = None # Union[None, Dict[str, Any]]
        self.drops = [] # List[Dict[str, Any]]

    def handle_starttag(self, tag: str, attrs: List[Tuple[str, str]]) -> None:
        self.current_tag = tag
        if tag == "table":
            classes = get_attr(attrs, "class")
            if classes is not None and "dropcharttbl" in classes:
                self.new_table = True
                self.in_table = True
        elif tag == "tr":
            self.new_row = True
            self.wide_row = False
            self.column = -1
        elif tag == "td":
            self.column += 1
            self.current_drop = None
            if self.new_row:
                colspan = get_attr(attrs, "colspan")
                if colspan is not None and colspan == '11':
                    self.wide_row = True
        elif self.in_table and tag == "abbr":
            title = get_attr(attrs, "title")
            if title is not None:
                m = RGX_RATE.search(title)
                if m is not None and self.current_drop is not None:
                    self.current_drop["rates"]["drop"] = float(m.group(1))
                    self.current_drop["rates"]["rare"] = float(m.group(2))

    def handle_endtag(self, tag: str) -> None:
        self.current_tag = None
        if tag == "table":
            self.in_table = False

    def handle_data(self, data: str) -> None:
        if not self.in_table:
            if self.current_tag == 'b' and data == "Boxes":
                self.box_table = True
            return

        if self.next_is_monster_name:
            self.next_is_monster_name = False
            if self.current_tag == 'br':
                self.current_monster = data.strip('/').strip()
                return

        if self.new_table and self.new_row and self.wide_row:
            m = RGX_EPISODE.search(data)
            if m is not None:
                self.current_episode = int(m.group(1))
            else:
                self.current_episode = 0
            self.new_table = False

        elif self.new_row and self.current_tag == 'u':
            if self.box_table:
                self.current_monster = data.strip() + " boxes"
            elif '/' in data:
                if self.difficulty == "Ultimate":
                    self.next_is_monster_name = True
                else:
                    self.current_monster = data.strip("/").strip()
            else:
                self.current_monster = data.strip()
            self.new_row = False

        elif self.current_tag == 'b':
            if data == "\u00a0":
                return
            self.current_drop = {
                "type": "box" if self.box_table else "monster",
                "item": data,
                "id": IDS[self.column - 1],
                "episode": self.current_episode,
                "monster": self.current_monster,
                "areas": get_areas(self.current_monster, self.current_episode),
                "difficulty": self.difficulty,
                "rates": {
                    "drop": 0.0,
                    "rare": 0.0,
                    "final": 0.0,
                }
            }
            self.drops.append(self.current_drop)

        elif self.current_tag == "sub" and self.current_drop is not None:
            self.current_drop["rates"]["final"] = float(data)

def get_drops(difficulty: str) -> List[Dict[str, Any]]:
    """ Retrieves the Ephinea drop chart for the given difficulty. """
    resp = requests.get(URL + difficulty.lower() + "/")
    parser = DropChartParser(difficulty)
    parser.feed(resp.content.decode("utf-8"))
    return parser.drops

def main():
    drops = [];
    for diff in DIFFICULTIES:
        drops += get_drops(diff)
    sys.stdout.write(json.dumps(drops, indent=2))

if __name__ == "__main__":
    main()

