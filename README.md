# Ephinea drop charts viewer

Browser-application that allows to view and filter the Ephinea PSOBB drop
charts.

A copy of this application is currently hosted at
<https://ephineadrops.netlify.app>.

## Refreshing the drops

Use the [helper script](get_drops.py) to generate the droplist JSON file to be
placed at [public/drops.json](public/drops.json).

The drops are scrapped from Ephinea's official HTML drop table. It is possible
the structure of the table change with time, so the script might break at any
point.

