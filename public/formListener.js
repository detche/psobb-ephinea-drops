/*
 * Implements a simple form listener mechanism for a callback to be called with
 * the form values once they are updated by the user.
 *
 * The update is done with a sliding timer so that it does not update too
 * often when the user is typing, etc.
 */

// Listen for the changes of the given form and call given callback with all
// its fields' data once it is updated. The callback is called with given delay.
// Currently only supports SELECT-MULTIPLE and TEXT INPUT.
function listenForm(form, delay, callback) {
    if (!form) {
        console.error("Cannot listen invalid form.");
        return;
    }

    // We keep the values in a single wrapper object, that we keep updated.
    let formValues = {};

    // Trigger for our callback, using a sliding timer if we have a delay.
    let trigger = (delay > 0
        ? slidingTrigger(() => callback(formValues), delay)
        : (_ = false) => callback(formValues));

    // Handler called when value of an element of the form is changed.
    const changeHandler = (e) => {
        formValues[e.target.id] = getValue(e.target);
        trigger();
    };

    // Listen to changes of all input elements of the form.
    let fields = [];
    for (let i = 0; i < form.elements.length; ++i) {
        let element = form.elements[i];

        if (!element.id) {
            continue;
        }

        if (element.tagName == "INPUT" || element.tagName == "SELECT") {
            // Save initial state of form input
            formValues[element.id] = getValue(element);

            // Handle changes in the future. We use the input event on text so
            // that user don't have to exit a field for an update to be
            // triggered.
            element.addEventListener(
                element.type == "text" ? "input" : "change",
                changeHandler);

            fields.push(element);
        }
    }

    // Also listen for form being reseted, in which case we update all values
    // with the form defaults.
    form.addEventListener("reset", () => {
        fields.forEach((field) => {
            formValues[field.id] = getValue(field, true); // set defaults
        });
        trigger(true);
    });

    // Perform initial update
    trigger(true);
}


// Returns a function that calls given callback after the given delay
// which slides after each trigger (until it is finally called).
// Can be executed directly by providing `true` as an argument.
function slidingTrigger(callback, delay = 500) {
    let id = null;

    return (now = false) => {
        if (id !== null) {
            clearTimeout(id);
            id = null;
        }
        if (now) {
            callback();
        } else {
            id = setTimeout(callback, delay);
        }
    };
}

// Returns the value of the given html element. If getDefault is True, the
// default value(s) of the element is/are returned. This is useful in case of
// resets.
function getValue(target, getDefault = false) {
    if (target) {
        if (target.selectedOptions !== undefined) {
            let values = [];
            if (getDefault) {
                for (let i = 0; i < target.options.length; ++i) {
                    let opt = target.options[i];
                    if (opt.defaultSelected) {
                        values.push(opt.value);
                    }
                }
            } else {
                for (let i = 0; i < target.selectedOptions.length; ++i) {
                    values.push(target.selectedOptions[i].value);
                }
            }
            return values;
        } else if (target.type === "checkbox") {
            return getDefault ? false : target.checked;
        } else if (target.value !== undefined) {
            let srcValue = (getDefault ? target.defaultValue : target.value);
            if (typeof srcValue === "string") {
                return srcValue.toLowerCase();
            }
            return srcValue;
        }
    }
}

