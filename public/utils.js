
// Returns a function that enables or disabled given html element.
function onOff(elementId, onState = "block") {
    let s = document.getElementById(elementId);

    return (state = true) => {
        if (!s) {
            s = document.getElementById(elementId);
        }
        if (s) {
            s.style.display = state ? onState : "none";
        }
    };
}

// Returns a function that sets the given HTML element's text.
function textSetter(elementId, onState = "block") {
    let s = document.getElementById(elementId);

    return (text) => {
        if (!s) {
            s = document.getElementById(elementId);
        }
        if (s) {
            s.style.display = text ? onState : "none";
            s.innerText = text ? text : "";
        }
    };
}

