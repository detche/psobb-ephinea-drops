/*
 * Implements retrieval of the Ephinea drops. They are then cached in user
 * browser for future retrivals.
 */

const DROPS_JSON_URL = "drops.json";
const CACHE_DATA_NAME = "psobb-drops";

// Returns a promise that resolves to the Ephinea PSOBB drops. They are cached
// in the browser local storage for faster access on page load, unelss
// forceRefresh is given.
function getDrops(forceRefresh = false) {
    return new Promise((resolve, reject) => {

        if (!forceRefresh) {
            let data = localStorage.getItem(CACHE_DATA_NAME);
            if (data !== null) {
                try {
                    resolve(JSON.parse(data));
                }
                catch(ex) {
                    reject("Uh oh, something went wrong. Try refreshing with the little button, bottom left of the page.");
                }
                return;
            }
        } else {
            localStorage.removeItem(CACHE_DATA_NAME);
        }

        getJson(DROPS_JSON_URL)
        .then(drops => {
            let data = {
                drops: drops,
                lastRefresh: (new Date()).toLocaleDateString(),
            };

            localStorage.setItem(CACHE_DATA_NAME, JSON.stringify(data));

            resolve(data);
        })
        .catch(err => reject(err));
    });
}

// Returns a promise that resolves on the json at given url.
function getJson(url) {
    return new Promise((resolve, reject) => {
        var req = new XMLHttpRequest();
        req.open("GET", url, true);

        req.onreadystatechange = () => {
            if (req.readyState < 4) {
                return;
            }

            if (req.status == 200) {
                if (req.responseType == "json") {
                    resolve(req.response);
                } else {
                    try {
                        resolve(JSON.parse(req.responseText));
                    } catch(ex) {
                        reject(ex.message);
                    }
                }

                return;
            }

            reject('server responded with code ' + req.status.toString());
        };

        req.send(null);
    });
}

