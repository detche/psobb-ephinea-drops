/*
 * Prints the drop charts, allowing to filter them.
 */

// Keep track of the lastly selected difficulty, so we can restore it when user
// change filters (if that difficulty is still available).
var selectedDifficulty = null;

// Options for the charts printer TODO: make them available to user
var printOptions = {
    printRates: true,
};

// Writes the given `drops` inside given `out` html element after applying
// given `filters`.
function printDropCharts(out, drops, filters) {
    return new Promise((resolve, reject) => {
        let results = 0;

        const junkList = getJunkItemList();
        // Function used to filter drops. We also count them.
        let filter = (drop) => {
            let ok = matches(filters.episode, drop.episode)
                && (!filters.hideBoxes || drop.type !== "box")
                && (!filters.hideJunk || !junkList.includes(drop.item))
                && matches(filters.sectionId, drop.id)
                && matches(filters.area, addChildren(drop.areas, filters.area))
                && matches(filters.monster, drop.monster)
                && matches(filters.item, drop.item);

            if (ok) {
                ++results;
            }

            return ok;
        };

        // We will build an index for our drops, based on our filter.
        // This is what will be printed afterwards.
        let data = prepareTableData(drops, filter);

        if (results > 0) {
            printDropTables(out, data, filters.area);
        }

        resolve(results);
    });
}

// Prepare and returns an index for the given doc meant to be used for the
// display of a drop chart in table format (with tabs for difficulties).
// filterFn is called for each drop and should returns whether or not we
// include it.
function prepareTableData(drops, filterFn) {
    let data = {}

    // Sort the drops into the matching categories
    drops.forEach(drop => {
        if (filterFn(drop)) {
            let ep = drop.episode.toString();

            // Index the drop, creating all intermediate elements as needed.
            // A same drop can be in two different areas, as some monster
            // exist in more than one place.
            drop.areas.forEach(area => {
                set(data,
                    [drop.difficulty,
                     ep,
                     "areas",
                     area,
                     drop.monster,
                     drop.id],
                    drop);
            });

            // Also save the section ids at the episode level for an easier
            // print process.
            if (data[drop.difficulty][ep]["sids"] === undefined) {
                data[drop.difficulty][ep]["sids"] = new Set();
            }
            data[drop.difficulty][ep]["sids"].add(drop.id);
        }
    });

    return data;
}

// Prints the drop tables into given output.
function printDropTables(out, tables, areaFilter) {
    let main = addElem(null, "DIV", e => {
        e.className = "drop-charts-wrapper";
    });

    let sel = addElem(main, "DIV", e => {
        e.className = "tab";
    });

    let tabs = addElem(main, "DIV", e => {
        e.className = "tabcontainer";
    });

    let tabHandlers = [];
    let newTabHandler = (difficulty) => {
        let handler = () => {
            // Hide all tabs
            forEach(tabs.children, e => show(e, false));

            // Deactivate all difficulty buttons, activate the new one.
            forEach(sel.children, e => {
                active(e, e.innerText == difficulty)
            });

            // Show current difficulty and make button active
            show(document.getElementById("tab" + difficulty));
            selectedDifficulty = difficulty;
        };

        // Save the name of the difficulty on the handler for later use.
        handler.difficultyName = difficulty;
        tabHandlers.push(handler);

        return handler;
    };

    // Create our drop charts
    DIFFICULTIES.forEach(diff => {
        if (!tables.hasOwnProperty(diff)) {
            return;
        }

        // Create the difficulty tab switcher buttons
        addElem(sel, "BUTTON", e => {
            e.setAttribute("class", "tablinks");
            e.addEventListener("click", newTabHandler(diff));
            e.innerText = diff;
        });

        // And the tab content for that difficulty
        addElem(tabs, "DIV", e => {
            e.setAttribute("class", "tabcontent");
            e.setAttribute("id", "tab" + diff);
            e.style.display = "none";

            printDropTable(e, tables[diff], areaFilter);
        });
    });

    // Display the finalized element.
    out.appendChild(main);

    // Now select the difficulty to be opened.
    let handlerToCall = null;

    forEach(tabHandlers, handler => {
        handlerToCall = handler; // we'll take the last one, biggest difficulty
        if (handler.difficultyName === selectedDifficulty) {
            return false; // but stop if we found the one previously selected.
        }
    });

    if (handlerToCall !== null) {
        handlerToCall();
    }
}

// Prints the drop table in given out element.
function printDropTable(out, data, areaFilter) {
    EPISODES.forEach(ep => {
        if (!data.hasOwnProperty(ep)) {
            return;
        }

        // Width in columns of the whole table, based on amount of section ids.
        const tableWidth = data[ep]["sids"].size + 1
        const sectionIds = [];

        // Create our drop table and all its child elems
        addElem(out, "TABLE", table => {
            table.className = "drop-table";

            // Head of the table, with episode number (and proper width)
            addElem(table, "THEAD", head => {
                addElem(head, "TR", row => {
                    addElem(row, "TH", cell => {
                        cell.innerText = "Episode " + ep;
                        cell.className = "episode episode-1";
                        cell.colSpan = tableWidth;
                    });
                });

                // Section ids header row
                addElem(head, "TR", row => {
                    addElem(row, "TH", e => e.className = "monster empty");

                    SECTION_IDS.forEach(sid => {
                        if (data[ep]["sids"].has(sid)) {
                            addElem(row, "TH", cell => {
                                cell.innerText = sid;
                                cell.className = "section-id "
                                    + sid.toLowerCase();
                            });

                            // also keep track of what ids we use, for drops
                            sectionIds.push(sid);
                        }
                    });
                });
            });

            // Now the body of our drop charts!
            addElem(table, "TBODY", body => {
                AREAS[ep].forEach(area => {
                    if (!data[ep]["areas"].hasOwnProperty(area)) {
                        return;
                    }

                    // Because the drops carry the information of all areas
                    // were it can appear, we need to apply the area filter here
                    // (if there is an area filter) so that we don't show areas
                    // the user don't want to see.
                    if (areaFilter !== ""
                        && area.toLowerCase().indexOf(areaFilter) === -1) {
                        return;
                    }

                    // Each area have a header with its name.
                    addElem(body, "TR", row => {
                        addElem(body, "TH", cell => {
                            cell.innerText = area;
                            cell.className = "area " + area.toLowerCase();
                            cell.colSpan = tableWidth;
                        });
                    });

                    // TODO: This assumes the monsters will come up in the
                    // right order in the drops. Maybe do the same as for areas
                    // and use an ordered list for the iteration.
                    for (let monster in data[ep]["areas"][area]) {
                        let mDrops = data[ep]["areas"][area][monster];

                        // Monster name
                        addElem(body, "TR", row => {
                            addElem(row, "TH", cell => {
                                cell.className = "monster";
                                cell.innerText = monster;
                            });

                            // Drops (empty cells if no item).
                            sectionIds.forEach(sid => {
                                addElem(row, "TD", cell => {
                                    cell.className = "drop " + sid.toLowerCase();
                                    if (mDrops.hasOwnProperty(sid)) {
                                        printDropCell(cell, mDrops[sid]);
                                    } else {
                                        cell.className += " empty";
                                    }
                                });
                            });
                        });
                    }
                });
            });
        });
    });
}

// Returns a properly formatted string representation of the given number.
function numStr(number) {
    if (isNaN(number)) {
        return "?";
    }

    let repr = number.toString();

    // Source: http://www.mredkj.com/javascript/numberFormat.html
	let x = repr.split('.');
	let x1 = x[0];
	let x2 = x.length > 1 ? '.' + x[1] : '';
	let rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}

	return x1 + x2;
}

// Print the given drop in a cell format, used in the context of printing the
// drop charts in tables.
function printDropCell(cellElement, drops) {
    if (!Array.isArray(drops)) {
        drops = [drops];
    }

    drops.forEach(drop => {
        addElem(cellElement, "DIV", itemDiv => {
            itemDiv.className = "drop-item";

            addElem(itemDiv, "SPAN", nameSpan => {
                nameSpan.className = "drop-name";
                nameSpan.innerText = drop.item;
            });

            if (printOptions.printRates) {
                addElem(itemDiv, "BR");
                addElem(itemDiv, "SPAN", rateSpan => {
                    rateSpan.className = "drop-rate";
                    printDropRate(rateSpan, drop);
                });
            }
        });
    });
}

// Round to the nth float digit.
function round(num, floats) {
    let p = Math.pow(10, floats);
    return Math.round(num * p) / p;
}

// Print the drop rate element inside given parent
function printDropRate(parent, drop) {
    // Use same format as Ephinea's drop table.
    addElem(parent, "ABBR", abbr => {
        if (drop.rates.drop && drop.rates.rare) {
            abbr.title =
                "Drop Rate: 1/" + numStr(drop.rates.drop)
                + " (" + numStr(round(100 / drop.rates.drop, 5)) + "%)\n"
                + "Rare Rate: 1/" + numStr(drop.rates.rare)
                + " (" + numStr(round(100 / drop.rates.rare, 5)) + "%)";
        }

        abbr.innerHTML = "<sup>1</sup>/<sub>"
            + numStr(Math.round(drop.rates.final))
            + "</sub>";
    });
}

// Returns whether given drop value matches the given filter.
function matches(filter, value) {
    if (filter === undefined || filter === null) {
        return true;
    }

    if (Array.isArray(filter)) {

        if (filter.length === 0) {
            return true; // consider empty list filter same as fully selected
        }

        if (Array.isArray(value)) {
            return value.some(v => filter.indexOf(v) !== -1);
        } else {
            return filter.indexOf(value.toString()) !== -1;
        }

    } else {

        if (typeof filter === "string" && filter.length === 0) {
            return true; // consider empty string as disabled filter
        }

        if (Array.isArray(value)) {
            for (let i = 0; i < value.length; ++i) {
                if (value.toString().toLowerCase().indexOf(filter.toString()) !== -1) {
                    return true;
                }
            }
            return false;
        } else {
            return value.toString().toLowerCase().indexOf(filter.toString()) !== -1;
        }
    }
}

// Sets the value of given object. If keys is an array, it represents all
// intermediate parents between the root object and the value. If they do not
// exist, they are created. Returns the input object.
function set(obj, keys, value) {
    if (typeof obj !== "object"
        || obj === null
        || keys === undefined || keys == null) {
        return obj;
    }

    let target = obj;
    if (!Array.isArray(keys)) {
        target[keys] = value;
    } else {
        for (let i = 0; i < keys.length; ++i) {
            let key = keys[i];

            if (i == keys.length - 1) {
                // Last key is the final destination of our value.
                if (!target[key]) {
                    target[key] = [];
                }

                target[key].push(value)
            } else {
                // Otherwise, access next object in the hierarchy, creating it
                // if it does not exist.
                if (target[key] === undefined) {
                    target[key] = {};
                }
                target = target[key]
            }
        }
    }

    return obj;
}

// Create and adds a new child to given parent and return that new element.
// If callback is given, it is called with new element. Callback is called
// BEFORE the new element is added to the parent.
function addElem(parent, elementTagName, initCb) {
    let newElem = document.createElement(elementTagName);
    if (initCb !== undefined) {
        initCb(newElem);
    }
    if (parent) {
        parent.appendChild(newElem);
    }
    return newElem;
}

// Activate (or not) the given element.
function active(element, activated = true) {
    if (element) {
        if (activated) {
            element.className += " active";
        } else {
            element.className = element.className.replace(" active", "");
        }
    }
}

// Show (or not) the given element.
function show(element, shown = true) {
    if (element) {
        element.style.display = shown ? "block" : "none";
    }
}

// Call given callback for each element of the collection.
// Iteration is interrupted if a call returns something. That something is
// returned by this function.
function forEach(elementCollection, callback) {
    for (let i = 0; i < elementCollection.length; ++i) {
        let result = callback(elementCollection[i]);
        if (result !== undefined)
            return result;
    }
}

// Takes a list of areas, and returns a copy with the parents of those areas
// added into that new list. This is only for parents that have a name different
// than the children, in which case they wouldn't normally be included.
function addChildren(areas, filter) {
    if (filter === "") {
        return areas; // do not do that thing if user gave no filter.
    }

    for (let i = 0; i < areas.length; ++i) {
        if (AREA_RELATIONS.hasOwnProperty(areas[i])) {
            return [...areas, ...AREA_RELATIONS[areas[i]]];
        }
    }

    return areas;
}
