function resetJunkItemList() {
    localStorage.removeItem("junk-item-list");
}

function updateJunkItemList(newList) {
    localStorage.setItem("junk-item-list", JSON.stringify(newList
        .split("\n")
        .map(i => i.trim())
        .filter(i => i)));
}

function getJunkItemList() {
    const customList = localStorage.getItem("junk-item-list");
    if (customList) {
        try {
            return JSON.parse(customList);
        } catch {
            return JUNK;
        }
    }

    return JUNK;
}

function openJunkItemListEditModal(onEdited = null) {
    const modalContainer = document.createElement("div");
    modalContainer.classList.add("modal-container");

    const modal = document.createElement("div");
    modal.classList.add("modal");

    const modalHeader = document.createElement("div");
    modalHeader.classList.add("modal-header");

    const modalTitle = document.createElement("div");
    modalTitle.innerText = "One item per line, list saved on close";
    modalTitle.classList.add("modal-title");
    modalHeader.appendChild(modalTitle);

    const modalClose = document.createElement("button");
    modalClose.classList.add("modal-close");
    modalHeader.appendChild(modalClose);

    const modalContent = document.createElement("div");
    modalContent.classList.add("modal-content");

    const itemList = getJunkItemList().join("\n");
    const listEditor = document.createElement("textarea");
    listEditor.classList.add("junk-item-list-editor");
    listEditor.value = itemList;
    modalContent.appendChild(listEditor);

    const resetButton = document.createElement("button");
    resetButton.innerText = "Reset to default list";
    resetButton.classList.add("reset-junk-item-list");
    modalContent.appendChild(resetButton);

    modal.appendChild(modalHeader);
    modal.appendChild(modalContent);
    modalContainer.appendChild(modal);

    let escapeListener = null;
    const close = () => {
        document.body.removeChild(modalContainer);
        if (escapeListener) {
            document.removeEventListener("keydown", escapeListener);
        }

        if (itemList != listEditor.value) {
            updateJunkItemList(listEditor.value);
            if (onEdited) {
                onEdited();
            }
        }
    };

    resetButton.addEventListener("click", () => {
        resetJunkItemList();
        listEditor.disabled = true;
        listEditor.value = itemList; // prevents update in close function
        close();
        if (onEdited) {
            onEdited();
        }
    });

    escapeListener = (e) => {
        if (e.key === "Escape") {
            close();
        }
    };

    document.body.appendChild(modalContainer);
    document.addEventListener("keydown", escapeListener);
    modalClose.addEventListener("click", close);
}

